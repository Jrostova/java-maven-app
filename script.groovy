def buildJar() {
    echo "building the application..."
    sh 'mvn package'
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t judedba/gs:jma-6.0 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push  judedba/gs:jma-6.0'
    }
} 
def testApp() {
    echo "Testing the application..."
}

def deployApp() {
    echo "deploying the application..."
} 

return this

